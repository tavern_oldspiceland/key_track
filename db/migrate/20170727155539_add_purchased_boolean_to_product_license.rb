class AddPurchasedBooleanToProductLicense < ActiveRecord::Migration[5.1]
  def change
    add_column :product_licenses, :purchased, :boolean
  end
end
