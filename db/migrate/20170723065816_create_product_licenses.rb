class CreateProductLicenses < ActiveRecord::Migration[5.1]
  def change
    create_table :product_licenses do |t|
      t.string :license_key
      t.string :product_name
      t.string :purchasing_client
      t.string :client_seat

      t.timestamps
    end
  end
end
