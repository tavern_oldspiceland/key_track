class AddServiceTagToProductLicenses < ActiveRecord::Migration[5.1]
  def change
    add_column :product_licenses, :service_tag, :string
  end
end
