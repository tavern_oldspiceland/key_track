class ProductLicense < ApplicationRecord
  # ProductLicense consists of:
  # :license_key, :product_name, :purchasing_client, :client_seat, :service_tag
  # :created_at, :updated_at, :purchased
  validates :service_tag, length: { is: 7 }
  # TODO: Is it really needed to validate length? What about non-dell PCs?
  validates :purchasing_client, presence: true
  # TODO: What if the license hasn't been purchased yet? Need to rethink
end
