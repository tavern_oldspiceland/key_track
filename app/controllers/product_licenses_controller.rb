class ProductLicensesController < ApplicationController
  before_action :set_product_license, only: [:show, :edit, :update, :destroy]

  # GET /product_licenses
  # GET /product_licenses.json
  def index
    @product_licenses = ProductLicense.all
  end

  # GET /product_licenses/1
  # GET /product_licenses/1.json
  def show
  end

  # GET /product_licenses/new
  def new
    @product_license = ProductLicense.new
  end

  # GET /product_licenses/1/edit
  def edit
  end

  # POST /product_licenses
  # POST /product_licenses.json
  def create
    @product_license = ProductLicense.new(product_license_params)

    respond_to do |format|
      if @product_license.save
        format.html { redirect_to @product_license, notice: 'Product license was successfully created.' }
        format.json { render :show, status: :created, location: @product_license }
      else
        format.html { render :new }
        format.json { render json: @product_license.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_licenses/1
  # PATCH/PUT /product_licenses/1.json
  def update
    respond_to do |format|
      if @product_license.update(product_license_params)
        format.html { redirect_to @product_license, notice: 'Product license was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_license }
      else
        format.html { render :edit }
        format.json { render json: @product_license.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_licenses/1
  # DELETE /product_licenses/1.json
  def destroy
    @product_license.destroy
    respond_to do |format|
      format.html {
        redirect_to product_licenses_url,
                    notice: 'Product license was successfully destroyed.'
      }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_product_license
    @product_license = ProductLicense.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def product_license_params
    params.require(:product_license).permit(:license_key, :product_name,
                                            :purchasing_client, :client_seat,
                                            :service_tag, :purchased)
  end
end
