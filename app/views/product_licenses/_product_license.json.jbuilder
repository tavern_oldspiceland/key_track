json.extract! product_license, :id, :license_key, :product_name, :purchasing_client, :client_seat, :created_at, :updated_at
json.url product_license_url(product_license, format: :json)
